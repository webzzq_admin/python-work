# 导入必要的库
import sys

import requests
from bs4 import BeautifulSoup
import pandas as pd
import os
from tenacity import sleep

#获取数字
def get_digit(text):
    numbers = ''.join(filter(str.isdigit, text))
    return numbers

# 处理每一页数据的函数
def process_page(url):
    # 发起GET请求，并加上请求头
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/129.0.0.0 Safari/537.36'
    }
    response = requests.get(url, headers=headers)

    # 解析HTML内容
    soup = BeautifulSoup(response.content, 'html.parser')

    # 判断就是否含有验证码
    captcha = soup.find('div',attrs={'id':'captcha'})

    if captcha is not None:
        print("出现验证码，稍后再爬取数据！")
        sys.exit(0)

    # 查找所有class为'info clear'的<div>标签
    info_divs = soup.find_all('div', class_='info clear')

    # 存储每套房子的信息的列表
    houses = []

    # 遍历每一个<div>标签
    for div in info_divs:
        # 获取标题
        title = div.find('div', class_='title').find('a').get_text()
        # 获取小区名称和地区板块
        position_info = div.find('div', class_='positionInfo').get_text()
        position_info_parts = position_info.split('-')
        community_name = position_info_parts[0].strip() if len(position_info_parts) > 0 else ''
        district = position_info_parts[1].strip() if len(position_info_parts) > 1 else ''
        # 获取户型、面积、朝向等信息
        house_info = div.find('div', class_='houseInfo').get_text()
        house_info_parts = house_info.split('|')
        house_type = house_info_parts[0].strip() if len(house_info_parts) > 0 else ''
        area = house_info_parts[1].strip() if len(house_info_parts) > 1 else ''
        orientation = house_info_parts[2].strip() if len(house_info_parts) > 2 else ''
        decoration = house_info_parts[3].strip() if len(house_info_parts) > 3 else ''
        floor = house_info_parts[4].strip() if len(house_info_parts) > 4 else ''

        # 获取关注信息
        follow_info = div.find('div',class_='followInfo').get_text()
        # 获取房屋总价和单价
        total_price = div.find('div', class_='totalPrice').get_text()
        total_price = get_digit(total_price)
        unit_price = div.find('div', class_='unitPrice').get_text()

        # 将信息添加到houses列表
        houses.append([title, community_name, district, house_type, area, orientation, decoration, floor, follow_info,
                       total_price, unit_price])

    # 返回houses列表
    return houses


# 处理所有100页数据的函数
def process_all_pages(base_url, total_pages):
    # 创建一个空的DataFrame来存储所有数据
    all_houses = pd.DataFrame(
        columns=["标题", "小区名", "地区板块", "户型", "面积", "朝向", "装修", "楼层", "关注信息", "房屋总价",
                 "房屋单价"])

    # 遍历每一页
    for page in range(1, total_pages + 1):
        url = f"{base_url}ershoufang/pg{page}/"
        houses = process_page(url)
        page_df = pd.DataFrame(houses, columns=["标题", "小区名", "地区板块", "户型", "面积", "朝向", "装修", "楼层",
                                                "关注信息", "房屋总价", "房屋单价"])
        all_houses = pd.concat([all_houses, page_df], ignore_index=True)
        print("爬取第'" + str(page) + "'页数据：")
        # 延迟爬取，预防反爬虫机制
        sleep(2)

    # 检查目录是否存在，如果不存在则创建
    directory = "D:/data"
    if not os.path.exists(directory):
        os.makedirs(directory)

    # 将DataFrame保存到Excel文件
    file_path = os.path.join(directory, "北京二手房数据.xlsx")
    all_houses.to_excel(file_path,index=False)


# 实际的网站基础URL
base_url = "https://bj.lianjia.com/"
total_pages = 100

# 运行函数
if __name__== '__main__':
    process_all_pages(base_url, total_pages)