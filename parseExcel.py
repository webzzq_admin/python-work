import pandas as pd
import matplotlib.pyplot as plt

# 读取csv文件
df = pd.read_excel('D:/data/北京二手房数据.xlsx')
house = pd.DataFrame(df)

plt.rcParams['font.sans-serif'] = ['KaiTi']  #中文
plt.rcParams['axes.unicode_minus'] = False   #负号


# 可视化
# 绘制总价的直方图
house.groupby("户型")["房屋总价"].mean().plot(kind="bar",color='orange')
plt.xlabel("户型")
plt.ylabel("平均总价")
plt.title("不同户型的平均总价柱状图")
plt.show()


